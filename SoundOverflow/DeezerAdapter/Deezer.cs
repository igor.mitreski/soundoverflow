//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Security.Cryptography;
//using System.Text;
//using System.Threading.Tasks;
//using System.IO;
//using System.Net.Http;
//using System.Security.Cryptography;
//using System.Security.Cryptography.X509Certificates;
//using System.Security.Cryptography.Xml;
//using System.Xml;
//using System.Xml.Linq;
//using System.Xml.Serialization;
//using System.Xml.XPath;
//using System.Xml.Xsl;
//using System.Xml.Schema;
//using System.Xml.Resolvers;
//using System.Xml.Serialization.Advanced;
//using System.Xml.Serialization.Configuration;
//using System.Xml.Serialization.GeneratedAssembly;
//using System.Xml.Serialization.TempAssembly;
//using System.Xml.Serialization.TypeScope;
//using System.Xml.Serialization.XmlCustomFormatter;
//using System.Xml.Serialization.XmlSerializationReader;
//using System.Xml.Serialization.XmlSerializationWriter;
//using System.Xml.Serialization.XmlTypeMapping;
//using System.Xml.Serialization.XmlSerializer;
//using System.Xml.Serialization.XmlSerializerFactory;
//using System.Xml.XPath;
//using System.Xml.Xsl;
//using System.Xml.Schema;
//using System.Xml.Resolvers;
//using System.Xml.Serialization.Advanced;
//using System.Xml.Serialization.Configuration;
//using System.Xml.Serialization.GeneratedAssembly;
//using System.Xml.Serialization.TempAssembly;
//using System.Xml.Serialization.TypeScope;
//using System.Xml.Serialization.XmlCustomFormatter;
//using System.Xml.Serialization.XmlSerializationReader;
//using System.Xml.Serialization.XmlSerializationWriter;
//using System.Xml.Serialization.XmlTypeMapping;
//using System.Xml.Serialization.XmlSerializer;
//using System.Xml.Serialization.XmlSerializerFactory;

//public class Deezer : DeezerPy
//{
//    public Deezer(string arl = null)
//    {
//        base.__init__();
//        if (arl != null)
//        {
//            this.arl = arl;
//            this.login_via_arl(arl);
//        }
//    }
//    public void login_via_arl(string arl, int child = 0)
//    {
//        base.login_via_arl(arl, child: child);
//        return this.current_user;
//    }
//    public object user
//    {
//        get
//        {
//            return this.current_user;
//        }
//    }
//    public Dictionary<string, object> get_track(string track_id, Dictionary<string, object> kwargs)
//    {
//        var data, m = this._api_fallback(partial(this.gw.get_track, track_id), partial(this.api.get_track, track_id), kwargs);
//        if (m == "gw")
//        {
//            data = util.map_gw_track(data);
//        }
//        else
//        {
//            data = util.map_api_track(data);
//        }
//        return new Dictionary<string, object>(){
//            {"info", data},
//            {"download", partial(this.download_track, data)},
//            {"tags", this.get_track_tags(data)},
//            {"get_tag", partial(this.get_track_tags, data)}
//        };
//    }
//    public List<object> get_track_valid_quality(Dictionary<string, object> track)
//    {
//        var qualities = new List<object>();
//        foreach (var key in new List<object>(){track_formats.MP3_128, track_formats.MP3_320, track_formats.FLAC})
//        {
//            var download_url = this.get_track_download_url(track, quality: key, fallback: false);
//            var res = this.session.get(download_url, stream: true);
//            if (res.status_code == 200 && int(res.headers["Content-length"]) > 0)
//            {
//                qualities.append(key);
//            }
//        }
//        return qualities;
//    }
//    public Dictionary<string, object> get_track_tags(Dictionary<string, object> track, string separator = ", ")
//    {
//        var album_data, _ = this.get_album(track["album"]["id"]);
//        var main_contributors = track.get("contributors", new List<object>()).Where(contributor => contributor["role"] == "Main").ToList();
//        var main_artists = track["artist"]["name"];
//        for (var i = 1; i < len(main_contributors); i++)
//        {
//            main_artists += separator + main_contributors[i]["name"];
//        }
//        var title = track.get("title");
//        if ("version" in track && track["version"] != "")
//        {
//            title += " " + track["version"];
//        }
//        bool should_include_featuring()
//        {
//            var feat_keywords = new List<string>(){ "feat.", "featuring", "ft." };
//            foreach (var keyword in feat_keywords)
//            {
//                if (keyword in title.ToLower())
//                {
//                    return false;
//                }
//            }
//            return true;
//        }
//        var featuring_artists_data = track.get("contributors", new List<object>()).Where(contributor => contributor["role"] == "Featured").ToList();
//        if (should_include_featuring() && len(featuring_artists_data) > 0)
//        {
//            var featuring_artists = featuring_artists_data[0]["name"];
//            for (var i = 1; i < len(featuring_artists_data); i++)
//            {
//                featuring_artists += separator + featuring_artists_data[i]["name"];
//            }
//            title += f" (feat. {featuring_artists})";
//        }
//        var total_tracks = album_data["nb_tracks"];
//        var track_number = str(track["track_number"]) + "/" + str(total_tracks);
//        var cover = this.get_album_poster(album_data, size: 1000);
//        var tags = new Dictionary<string, object>(){
//            {"title", title},
//            {"artist", main_artists},
//            {"genre", null},
//            {"album", album_data.get("title")},
//            {"albumartist", album_data.get("artist")["name"]},
//            {"label", album_data.get("label")},
//            {"date", track.get("release_date")},
//            {"discnumber", track.get("disk_number")},
//            {"tracknumber", track_number},
//            {"isrc", track.get("isrc")},
//            {"copyright", album_data.get("copyright")},
//            {"_albumart", cover},
//        };
//        if (len(album_data["genres"]["data"]) > 0)
//        {
//            tags["genre"] = album_data["genres"]["data"][0]["name"];
//        }
//        var _authors = track.get("contributors", new List<object>()).Where(contributor => contributor["role"] == "Author").ToList();
//        if (len(_authors) > 0)
//        {
//            var authors = _authors[0]["name"];
//            for (var i = 1; i < len(_authors); i++)
//            {
//                authors += separator + _authors[i]["name"];
//            }
//            tags["author"] = authors;
//        }
//        return tags;
//    }
//    public string get_track_download_url(Dictionary<string, object> track, object quality = null, bool fallback = true, bool renew = false, Dictionary<string, object> kwargs)
//    {
//        if (renew)
//        {
//            track = this.get_track(track["id"])["info"];
//        }
//        if (quality == null)
//        {
//            quality = track_formats.MP3_128;
//            fallback = true;
//        }
//        try
//        {
//            if (!"md5_origin" in track)
//            {
//                throw new DownloadLinkDecryptionError("MD5 is needed to decrypt the download link.");
//            }
//            var md5_origin = track["md5_origin"];
//            var track_id = track["id"];
//            var media_version = track["media_version"];
//        }
//        catch (ValueError)
//        {
//            throw new ValueError("You have passed an invalid argument.");
//        }
//        string decrypt_url(object quality_code)
//        {
//            var magic_char = "¤";
//            var step1 = magic_char.join((md5_origin, str(quality_code), track_id, media_version));
//            var m = hashlib.md5();
//            m.update(bytes([ord(x) for x in step1]));
//            var step2 = m.hexdigest() + magic_char + step1 + magic_char;
//            step2 = step2.ljust(80, " ");
//            var cipher = Cipher(algorithms.AES(bytes('jo6aey6haid2Teih', 'ascii')), modes.ECB(), default_backend());
//            var encryptor = cipher.encryptor();
//            var step3 = encryptor.update(bytes([ord(x) for x in step2])).hex();
//            var cdn = track["md5_origin"][0];
//            return f'https://e-cdns-proxy-{cdn}.dzcdn.net/mobile/1/{step3}';
//        }
//        var url = decrypt_url(track_formats.TRACK_FORMAT_MAP[quality]["code"]);
//        var res = this.session.get(url, stream: true);
//        if (!fallback || (res.status_code == 200 && int(res.headers["Content-length"]) > 0))
//        {
//            res.close();
//            return (url, quality);
//        }
//        else
//        {
//            if ("fallback_qualities" in kwargs)
//            {
//                var fallback_qualities = kwargs["fallback_qualities"];
//            }
//            else
//            {
//                var fallback_qualities = track_formats.FALLBACK_QUALITIES;
//            }
//            foreach (var key in fallback_qualities)
//            {
//                url = decrypt_url(track_formats.TRACK_FORMAT_MAP[key]["code"]);
//                res = this.session.get(url, stream: true);
//                if (res.status_code == 200 && int(res.headers["Content-length"]) > 0)
//                {
//                    res.close();
//                    return (url, key);
//                }
//            }
//        }
//    }
//    public void download_track(Dictionary<string, object> track, string download_dir, object quality = null, bool fallback = true, string filename = null, bool renew = false, bool with_metadata = true, bool with_lyrics = true, string tag_separator = ", ", bool show_messages = true, BaseProgressHandler progress_handler = null, Dictionary<string, object> kwargs)
//    {
//        if (with_lyrics)
//        {
//            try
//            {
//                var lyrics_data = track.get("lyrics", this.get_track_lyrics(track["id"])["info"]);
//            }
//            catch (Exception)
//            {
//                with_lyrics = false;
//            }
//        }
//        var tags = this.get_track_tags(track, separator: tag_separator);
//        var url, quality_key = this.get_track_download_url(track, quality, fallback: fallback, renew: renew, kwargs);
//        var blowfish_key = util.get_blowfish_key(track["id"]);
//        var quality = track_formats.TRACK_FORMAT_MAP[quality_key];
//        var title = tags["title"];
//        var ext = quality["ext"];
//        if (filename == null)
//        {
//            filename = title + ext;
//        }
//        if (!str(filename).endswith(ext))
//        {
//            filename += ext;
//        }
//        filename = util.clean_filename(filename);
//        download_dir = path.normpath(download_dir);
//        var download_path = path.join(download_dir, filename);
//        util.create_folders(download_dir);
//        if (show_messages)
//        {
//            Console.WriteLine("Starting download of:", title);
//        }
//        var res = this.session.get(url, stream: true);
//        var chunk_size = 2048;
//        var total_filesize = int(res.headers["Content-Length"]);
//        var i = 0;
//        var data_iter = res.iter_content(chunk_size);
//        if (progress_handler == null)
//        {
//            progress_handler = DefaultProgressHandler();
//        }
//        progress_handler.initialize(data_iter, title, quality_key, total_filesize, chunk_size, track_id: track["id"]);
//        using (var f = new FileStream(download_path, FileMode.Create, FileAccess.Write))
//        {
//            f.Seek(0, SeekOrigin.Begin);
//            foreach (var chunk in data_iter)
//            {
//                var current_chunk_size = len(chunk);
//                if (i % 3 > 0)
//                {
//                    f.Write(chunk);
//                }
//                else if (len(chunk) < chunk_size)
//                {
//                    f.Write(chunk);
//                    progress_handler.update(track_id: track["id"], current_chunk_size: current_chunk_size);
//                    break;
//                }
//                else
//                {
//                    var cipher = Cipher(algorithms.Blowfish(blowfish_key), modes.CBC(bytes([i for i in range(8)])), default_backend());
//                    var decryptor = cipher.decryptor();
//                    var dec_data = decryptor.update(chunk) + decryptor.finalize();
//                    f.Write(dec_data);
//                    current_chunk_size = len(dec_data);
//                }
//                i += 1;
//                progress_handler.update(track_id: track["id"], current_chunk_size: current_chunk_size);
//            }
//        }
//        if (with_metadata)
//        {
//            if (ext.ToLower() == ".flac")
//            {
//                this._write_flac_tags(download_path, track, tags: tags);
//            }
//            else
//            {
//                this._write_mp3_tags(download_path, track, tags: tags);
//            }
//        }
//        if (with_lyrics)
//        {
//            var lyrics_path = path.join(download_dir, filename[:-len(ext)]);
//            this.save_lyrics(lyrics_data, lyrics_path);
//        }
//        if (show_messages)
//        {
//            Console.WriteLine("Track downloaded to:", download_path);
//        }
//        progress_handler.close(track_id: track["id"], total_filesize: total_filesize);
//    }
//    public object get_tracks(List<object> track_ids)
//    {
//        return this.gw.get_tracks_gw(track_ids);
//    }
//    public Dictionary<string, object> get_track_lyrics(string track_id)
//    {
//        var data = this.gw.get_track_lyrics(track_id);
//        return new Dictionary<string, object>(){
//            {"info", data},
//            {"save", partial(this.save_lyrics, data)}
//        };
//    }
//    public bool save_lyrics(Dictionary<string, object> lyric_data, string save_path)
//    {
//        var filename = path.basename(save_path);
//        filename = util.clean_filename(filename);
//        save_path = path.join(path.dirname(save_path), filename);
//        if (!str(save_path).endswith(".lrc"))
//        {
//            save_path += ".lrc";
//        }
//        util.create_folders(path.dirname(save_path));
//        using (var f = new StreamWriter(save_path, false, Encoding.UTF8))
//        {
//            if (!"LYRICS_SYNC_JSON" in lyric_data)
//            {
//                return false;
//            }
//            var sync_data = lyric_data["LYRICS_SYNC_JSON"];
//            foreach (var line in sync_data)
//            {
//                if (str(line["line"]))
//                {
//                    f.Write("{0}{1}".format(line["lrc_timestamp"], line["line"]));
//                }
//                f.Write("\n");
//            }
//        }
//        return true;
//    }
//    public Dictionary<string, object> get_album(string album_id)
//    {
//        var data, m = this._api_fallback(partial(this.gw.get_album, album_id), partial(this.api.get_album, album_id), gw_priority: false);
//        if (m == "gw")
//        {
//            data = util.map_gw_album(data);
//        }
//        data["cover_id"] = str(data["cover_small"]).split("cover/")[1].split("/")[0];
//        return data, m;
//    }
//    public string get_album_poster(Dictionary<string, object> album, int size = 500, string ext = "jpg")
//    {
//        return this._get_poster(album["cover_id"], size: size, ext: ext);
//    }
//    public object get_album_tracks(string album_id)
//    {
//        return this.gw.get_album_tracks(album_id);
//    }
//    public object get_artist(string artist_id)
//    {
//        return this.gw.get_artist(artist_id);
//    }
//    public string get_artist_poster(object artist, int size = 500, string ext = "jpg")
//    {
//        if (!"ART_PICTURE" in artist && "DATA" in artist)
//        {
//            artist = artist["DATA"];
//        }
//        return this._get_poster(artist["ART_PICTURE"], size: size, ext: ext);
//    }
//    public object get_artist_discography(string artist_id, Dictionary<string, object> kwargs)
//    {
//        return this.gw.get_artist_discography(artist_id, kwargs);
//    }
//    public object get_artist_top_tracks(string artist_id, Dictionary<string, object> kwargs)
//    {
//        return this.gw.get_artist_top_tracks(artist_id, kwargs);
//    }
//    public object get_playlist(string playlist_id)
//    {
//        var result, m = this._api_fallback(partial(this.gw.get_playlist, playlist_id), partial(this.api.get_playlist, playlist_id));
//        if (m == "gw")
//        {
//            result["mapped"] = util.map_playlist(result);
//        }
//        return result;
//    }
//    public object get_playlist_tracks(string playlist_id)
//    {
//        return this.gw.get_playlist_tracks(playlist_id);
//    }
//    public object get_suggested_queries(string query)
//    {
//        var data = this.gw.api_call("search_getSuggestedQueries", params: new Dictionary<string, object>(){
//            {"QUERY", query}
//        });
//        var results = data["SUGGESTION"];
//        foreach (var result in results)
//        {
//            if ("HIGHLIGHT" in result)
//            {
//                del result["HIGHLIGHT"];
//            }
//        }
//        return results;
//    }
//    public object search_tracks(string query, Dictionary<string, object> kwargs)
//    {
//        return this.api.search_track(query, kwargs);
//    }
//}


