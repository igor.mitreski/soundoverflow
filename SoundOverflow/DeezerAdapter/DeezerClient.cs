﻿using Newtonsoft.Json.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace DeezerAdapter
{
    public class DeezerClient
    {
        

        public void DownloadMP3(string url, string savePath)
        {
            try
            {
                // Create a WebClient to download the file.
                using (WebClient client = new WebClient())
                {
                    // Download the MP3 file and save it to the specified path.
                    client.DownloadFile(url, savePath);
                }

                Console.WriteLine("MP3 file downloaded successfully.");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error downloading MP3 file: {ex.Message}");
            }
        }

        string DecryptUrl(int qualityCode, string md5Origin, string trackId, string mediaVersion)
        {
            char magicChar = '¤';
            string step1 = string.Join(magicChar.ToString(), md5Origin, qualityCode.ToString(), trackId, mediaVersion);
            using (MD5 m = MD5.Create())
            {
                byte[] step1Bytes = Encoding.ASCII.GetBytes(step1);
                byte[] step2Bytes = m.ComputeHash(step1Bytes);
                string step2 = BitConverter.ToString(step2Bytes).Replace("-", "").ToLower() + magicChar + step1.PadRight(80);
                using (Aes aesAlg = Aes.Create())
                {
                    aesAlg.Key = Encoding.ASCII.GetBytes("jo6aey6haid2Teih");
                    aesAlg.Mode = CipherMode.ECB;
                    ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
                    byte[] step3Bytes = encryptor.TransformFinalBlock(Encoding.ASCII.GetBytes(step2), 0, step2.Length);
                    string step3 = BitConverter.ToString(step3Bytes).Replace("-", "").ToLower();
                    char cdn = md5Origin[0];
                    return $"https://e-cdns-proxy-{cdn}.dzcdn.net/mobile/1/{step3}";
                }
            }
        }

        public class Track
        {
            public string Id { get; set; }
            public Dictionary<string, string> Info { get; set; }
        }

        public class TrackFormats
        {
            public static string MP3_128 = "MP3_128";
            public static Dictionary<string, TrackFormat> TRACK_FORMAT_MAP = new Dictionary<string, TrackFormat>
            {
                // Populate with actual track format map
            };

            public static string[] FALLBACK_QUALITIES = new string[]
            {
                // Populate with actual fallback qualities
            };
        }

        public class TrackFormat
        {
            public int Code { get; set; }
        }

        public class DownloadLinkDecryptionError : Exception
        {
            public DownloadLinkDecryptionError(string message) : base(message) { }
        }
    }
}
