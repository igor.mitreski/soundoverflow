using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace DeezerAdapter { 
public class Utils
{
    public static Dictionary<string, int> RELEASE_TYPE = new Dictionary<string, int>()
    {
        {"single", 0},
        {"album", 1},
        {"compile", 2},
        {"ep", 3},
        {"bundle", 4}
    };

    public static Dictionary<string, string> ROLE_MAP = new Dictionary<string, string>()
    {
        {"0", "Main"},
        {"5", "Featured"}
    };

    public static void Main(string[] args)
    {
        Dictionary<string, object> track = new Dictionary<string, object>()
        {
            {"ALB_ID", "album_id"},
            {"ALB_PICTURE", "album_md5_cover"},
            {"ALB_TITLE", "album_title"},
            {"ARTISTS", new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object>()
                    {
                        {"ART_ID", "artist_id"},
                        {"ART_PICTURE", "artist_picture"},
                        {"ART_NAME", "artist_name"},
                        {"ROLE_ID", "role_id"},
                        {"ARTIST_IS_DUMMY", "is_dummy"},
                        {"RANK", "rank"},
                        {"LOCALES", "locales"},
                        {"SMARTRADIO", "smartradio"},
                        {"__TYPE__", "type"}
                    }
                }
            },
            {"DIGITAL_RELEASE_DATE", "digital_release_date"},
            {"PHYSICAL_RELEASE_DATE", "physical_release_date"},
            {"EXPLICIT_TRACK_CONTENT", new Dictionary<string, object>()
                {
                    {"EXPLICIT_LYRICS_STATUS", "lyrics"},
                    {"EXPLICIT_COVER_STATUS", "cover"}
                }
            },
            {"MEDIA", new List<Dictionary<string, object>>()
                {
                    new Dictionary<string, object>()
                    {
                        {"TYPE", "preview"},
                        {"HREF", "preview_href"}
                    }
                }
            },
            {"SNG_ID", "id"},
            {"SNG_TITLE", "title"},
            {"MD5_ORIGIN", "md5_origin"},
            {"USER_ID", "user_id"},
            {"TRACK_NUMBER", "track_number"},
            {"DISK_NUMBER", "disk_number"},
            {"DURATION", "duration"},
            {"EXPLICIT_LYRICS", "explicit_lyrics"},
            {"GENRE_ID", "genre_id"},
            {"HIERARCHICAL_TITLE", "hierarchical_title"},
            {"ISRC", "isrc"},
            {"LYRICS_ID", "lyrics_id"},
            {"PROVIDER_ID", "provider_id"},
            {"RANK", "rank"},
            {"SMARTRADIO", "smartradio"},
            {"STATUS", "status"},
            {"VERSION", "version"},
            {"GAIN", "gain"},
            {"MEDIA_VERSION", "media_version"},
            {"TRACK_TOKEN", "token"},
            {"TRACK_TOKEN_EXPIRE", "token_expire"},
            {"__TYPE__", "type"}
        };

        Dictionary<string, object> album = new Dictionary<string, object>()
        {
            {"__TYPE__", "album_type"},
            {"EXPLICIT_ALBUM_CONTENT", new Dictionary<string, object>()
                {
                    {"EXPLICIT_LYRICS_STATUS", "explicit_lyrics"}
                }
            },
            {"LABEL_NAME", "label_name"},
            {"ART_ID", "artist_id"},
            {"ART_NAME", "artist_name"},
            {"COPYRIGHT", "copyright"}
        };

        Dictionary<string, object> query = new Dictionary<string, object>()
        {
            {"feat[.]?", " "},
            {"ft[.]?", " "},
            {"\\(feat[.]?", " "},
            {"\\(ft[.]?", " "},
            {"\\&", ""},
            {"–", "-"}
        };

        Dictionary<string, object> trackMapped = MapGwTrack(track);
        Dictionary<string, object> apiTrackMapped = MapApiTrack(track);
        //Dictionary<string, object> gwAlbumMapped = MapGwAlbum(album);
        string cleanedQuery = CleanQuery(query);
        string directory = "directory";
        CreateFolders(directory);
        //string cleanedFilename = CleanFilename("filename");
        string text = "text";
        string textMd5 = GetTextMd5(text);
        string blowfishKey = GetBlowfishKey("track_id");
    }

    public static Dictionary<string, object> MapGwTrack(Dictionary<string, object> track)
    {
        string albumId = (string)track["ALB_ID"];
        string albumMd5Cover = (string)track["ALB_PICTURE"];
        Dictionary<string, object> album = new Dictionary<string, object>()
        {
            {"id", albumId},
            {"md5_image", albumMd5Cover}
        };
        album["title"] = (string)track["ALB_TITLE"];
        album["cover"] = $"https://api.deezer.com/album/{albumId}/image";
        album["cover_small"] = $"https://e-cdns-images.dzcdn.net/images/cover/{albumMd5Cover}/56x56-000000-80-0-0.jpg";
        album["cover_medium"] = $"https://e-cdns-images.dzcdn.net/images/cover/{albumMd5Cover}/250x250-000000-80-0-0.jpg";
        album["cover_big"] = $"https://e-cdns-images.dzcdn.net/images/cover/{albumMd5Cover}/500x500-000000-80-0-0.jpg";
        album["cover_xl"] = $"https://e-cdns-images.dzcdn.net/images/cover/{albumMd5Cover}/1000x1000-000000-80-0-0.jpg";
        List<Dictionary<string, object>> contributors = new List<Dictionary<string, object>>();
        foreach (Dictionary<string, object> art in (List<Dictionary<string, object>>)track["ARTISTS"])
        {
            string id = (string)art["ART_ID"];
            string p = (string)art["ART_PICTURE"];
            contributors.Add(new Dictionary<string, object>()
            {
                {"id", id},
                {"name", (string)art["ART_NAME"]},
                {"role", ROLE_MAP.GetValueOrDefault((string)art["ROLE_ID"], "Unknown")},
                {"is_dummy", art["ARTIST_IS_DUMMY"]},
                {"picture", $"https://api.deezer.com/artist/{id}/image"},
                {"picture_small", $"https://e-cdns-images.dzcdn.net/images/artist/{p}/56x56-000000-80-0-0.jpg"},
                {"picture_medium", $"https://e-cdns-images.dzcdn.net/images/artist/{p}/250x250-000000-80-0-0.jpg"},
                {"picture_big", $"https://e-cdns-images.dzcdn.net/images/artist/{p}/500x500-000000-80-0-0.jpg"},
                {"picture_xl", $"https://e-cdns-images.dzcdn.net/images/artist/{p}/1000x1000-000000-80-0-0.jpg"},
                {"rank", art["RANK"]},
                {"locales", art["LOCALES"]},
                {"smartradio", art["SMARTRADIO"]},
                {"type", art["__TYPE__"]}
            });
        }
        Dictionary<string, object> mainArtist = contributors.FirstOrDefault(contributor => (string)contributor["role"] == "Main");
        string digitalReleaseDate = (string)track["DIGITAL_RELEASE_DATE"];
        string physicalReleaseDate = (string)track["PHYSICAL_RELEASE_DATE"];
        string releaseDate = physicalReleaseDate ?? digitalReleaseDate;
        Dictionary<string, object> explicitTrackContent = (Dictionary<string, object>)track["EXPLICIT_TRACK_CONTENT"];
        Dictionary<string, object> explicitContent = null;
        if (explicitTrackContent != null)
        {
            explicitContent = new Dictionary<string, object>()
            {
                {"lyrics", explicitTrackContent["EXPLICIT_LYRICS_STATUS"]},
                {"cover", explicitTrackContent["EXPLICIT_COVER_STATUS"]}
            };
        }
        string preview = null;
        foreach (Dictionary<string, object> medium in (List<Dictionary<string, object>>)track["MEDIA"])
        {
            if ((string)medium["TYPE"] == "preview")
            {
                preview = (string)medium["HREF"];
                break;
            }
        }
        return new Dictionary<string, object>()
        {
            {"id", track["SNG_ID"]},
            {"title", track["SNG_TITLE"]},
            {"album", album},
            {"contributors", contributors},
            {"artist", mainArtist},
            {"md5_origin", track["MD5_ORIGIN"]},
            {"user_id", track["USER_ID"]},
            {"digital_release_date", digitalReleaseDate},
            {"physical_release_date", physicalReleaseDate},
            {"release_date", releaseDate},
            {"track_number", track["TRACK_NUMBER"]},
            {"disk_number", track["DISK_NUMBER"]},
            {"duration", track["DURATION"]},
            {"explicit_lyrics", Convert.ToInt32(track["EXPLICIT_LYRICS"]) > 0},
            {"explicit_content", explicitContent},
            {"genre_id", track["GENRE_ID"]},
            {"hierarchical_title", track["HIERARCHICAL_TITLE"]},
            {"isrc", track["ISRC"]},
            {"lyrics_id", track["LYRICS_ID"]},
            {"provider_id", track["PROVIDER_ID"]},
            {"rank", track["RANK"]},
            {"smartradio", track["SMARTRADIO"]},
            {"status", track["STATUS"]},
            {"version", track["VERSION"]},
            {"gain", track["GAIN"]},
            {"media_version", track["MEDIA_VERSION"]},
            {"token", track["TRACK_TOKEN"]},
            {"token_expire", track["TRACK_TOKEN_EXPIRE"]},
            {"preview", preview},
            {"type", track["__TYPE__"]}
        };
    }

    public static Dictionary<string, object> MapApiTrack(Dictionary<string, object> track)
    {
        return new Dictionary<string, object>()
        {
            {"id", track["id"]},
            {"title", track["title"]},
            {"isrc", track["isrc"]},
            {"duration", track["duration"]},
            {"track_number", track["track_position"]},
            {"disk_number", track["disk_number"]},
            {"rank", track["rank"]},
            {"release_date", track["release_date"]},
            {"explicit_lyrics", track["explicit_lyrics"]},
            {"explicit_content", new Dictionary<string, object>()
                {
                    {"lyrics", track["explicit_content_lyrics"]},
                    {"cover", track["explicit_content_cover"]}
                }
            },
            {"preview", track["preview"]},
            {"gain", track["gain"]},
            {"contributors", track["contributors"]},
            {"artist", track["artist"]},
            {"album", track["album"]},
            {"type", track["type"]}
        };
    }


    public static string CleanQuery(Dictionary<string, object> query)
    {
        string queryStr = "query";
        foreach (KeyValuePair<string, object> entry in query)
        {
            queryStr = Regex.Replace(queryStr, entry.Key, (string)entry.Value);
        }
        return queryStr;
    }

    public static void CreateFolders(string directory)
    {
        directory = Path.GetFullPath(directory);
        DirectoryInfo di = Directory.CreateDirectory(directory);
    }

    //public static string CleanFilename(string filename)
    //{
    //    string whitelist = CLEAN_FILENAME_WHITELIST.Keys.First();
    //    string whitelistChars = CLEAN_FILENAME_WHITELIST.Values.First();
    //    int charLimit = 255;
    //    string replace = "";
    //    foreach (char r in replace)
    //    {
    //        filename = filename.Replace(r, '_');
    //    }
    //    string cleanedFilename = Encoding.ASCII.GetString(Encoding.GetEncoding("UTF-8").GetBytes(filename.Normalize(NormalizationForm.FormKD)));
    //    cleanedFilename = new string(cleanedFilename.Where(c => whitelistChars.Contains(c)).ToArray());
    //    if (cleanedFilename.Length > charLimit)
    //    {
    //        Console.WriteLine($"Warning, filename truncated because it was over {charLimit}. Filenames may no longer be unique");
    //    }
    //    return cleanedFilename.Substring(0, Math.Min(cleanedFilename.Length, charLimit));
    //}

    public static string GetTextMd5(string text, string encoding = "UTF-8")
    {
        using (MD5 md5 = MD5.Create())
        {
            byte[] hash = md5.ComputeHash(Encoding.GetEncoding(encoding).GetBytes(text));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }
    }

    public static string GetBlowfishKey(string trackId)
    {
        string secret = "g4el58wc0zvf9na1";
        using (MD5 md5 = MD5.Create())
        {
            byte[] idBytes = Encoding.ASCII.GetBytes(trackId);
            byte[] idMd5 = md5.ComputeHash(idBytes);
            byte[] blowfishKey = new byte[16];
            for (int i = 0; i < 16; i++)
            {
                blowfishKey[i] = (byte)(idMd5[i] ^ idMd5[i + 16] ^ Encoding.ASCII.GetBytes(secret)[i]);
            }
            return Encoding.ASCII.GetString(blowfishKey);
        }
    }
    }
}


