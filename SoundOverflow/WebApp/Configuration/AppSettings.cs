﻿namespace WebApp.Configuration
{
    public class AppSettings
    {
        public string RootDomain { get; set; }
        public DeezerSettings DeezerSettings { get; set; }
    }
}
