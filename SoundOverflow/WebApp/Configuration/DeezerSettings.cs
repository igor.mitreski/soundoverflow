﻿namespace WebApp.Configuration
{
    public class DeezerSettings
    {
        public string CallbackUrl { get; set; }
        public string AppId { get; set; }
        public string SecretKey { get; set; }
        public string DialogUrl { get; set; }
        public string TokenUrl { get; set; }
    }
}
