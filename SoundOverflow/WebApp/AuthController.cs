﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Specialized;
using System.Net;
using System.Web;
using E.Deezer;

namespace WebApp
{
    public class AuthController : BaseController
    {
        private string appId = "662651";
        private string appSecret = "d88c35cae86db1c6f2f75cc091e0fbd7";
        private string callbackUrl = "https://hitechub.com:7208/Auth";
        private string state;

        public async Task<IActionResult> Index()
        {
            string code = HttpContext.Request.Query["code"];
            if (string.IsNullOrEmpty(code))
            {
                state = Guid.NewGuid().ToString("N"); // CSRF protection
                HttpContext.Session.SetString("state", state);
                string dialogUrl = $"https://connect.deezer.com/oauth/auth.php?app_id={appId}&redirect_uri={HttpUtility.UrlEncode(callbackUrl)}&perms=email,offline_access&state={state}";
                return Redirect(dialogUrl);
            }
            else if (HttpContext.Request.Query["state"] == HttpContext.Session.GetString("state"))
            { 
                DeezerObject = DeezerSession.CreateNew();
                string tokenUrl = $"https://connect.deezer.com/oauth/access_token.php?app_id={appId}&secret={appSecret}&code={code}";
                using (WebClient client = new WebClient())
                {
                    string response = client.DownloadString(tokenUrl);
                    NameValueCollection queryParams = HttpUtility.ParseQueryString(response);
                    string accessToken = queryParams["access_token"];
                    await DeezerObject.Login(accessToken);
                    HttpContext.Session.SetString("AccessToken", accessToken);
                }
            }

            var user = DeezerObject.User;

            return null;
        }
    }
}
