﻿using E.Deezer;
using Microsoft.AspNetCore.Mvc;

namespace WebApp
{
    public class BaseController : Controller
    {
        public Deezer DeezerObject { get; set; }
    }
}
